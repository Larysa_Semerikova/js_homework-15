let number = +prompt("Enter your number");
while (number === "" || !number) {
    number = +prompt("Enter your number", number);
  }

function factorial(n) {
    return (n != 1) ? n * factorial(n - 1) : 1;
  }

alert(factorial(number))